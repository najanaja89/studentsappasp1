﻿using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service
{
    public class StudentsService : IService
    {
        public void AddStudent(Student student, List<Student> students)
        {
            students.Add(student);
        }

        public double AverageGradeByGroup(List<Student> students)
        {
            var avg = students.Select(s => s.GPA).Average();
            return avg;
        }

        public string BestStudent(List<Student> students)
        {
            var max = students.Select(s => s.GPA).Max();
            var student = students.Where(s => s.GPA == max).FirstOrDefault();

            return student.Name;
        }

        public Student CreateStudent(string name, double gpa)
        {
            Student student = new Student(name, gpa);
            return student;
        }

        public string WorstStudent(List<Student> students)
        {
            var min = students.Select(s => s.GPA).Min();
            var student = students.Where(s => s.GPA == min).FirstOrDefault();

            return student.Name;
        }
    }
}
