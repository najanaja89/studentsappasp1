﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    public interface IService
    {
        Student CreateStudent(string name, double gpa);
        void AddStudent(Student student, List<Student> students);
        double AverageGradeByGroup(List<Student> students);
        string WorstStudent(List<Student> students);
        string BestStudent(List<Student> students);
    }
}
