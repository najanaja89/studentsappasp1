﻿using Models;
using Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentsApp
{
    class Program
    {
        static void Main(string[] args)
        {
            var service = new StudentsService();
            List<Student> students = new List<Student>
            {
                new Student{ Name="Bruce", GPA=5.0},
                new Student{ Name="Barry", GPA=3.4},
                new Student{ Name="Jessie", GPA=2.4},
                new Student{ Name="Barbara", GPA=4.4},
                new Student{ Name="Jim", GPA=1.4}
            };
            while (true)
            {
                string menu;
                Console.WriteLine("Press 1 to Add Student");
                Console.WriteLine("Press 2 to See Worst Student");
                Console.WriteLine("Press 3 to See Best Student");
                Console.WriteLine("Press 4 to See Average GPA by group");
                Console.WriteLine("Press 5 to exit");

                menu = Console.ReadLine();

                switch (menu)
                {
                    case "1":
                        string name = " ";
                        while (true)
                        {
                            name = "";
                            double gpa = 0;
                            Console.WriteLine("Enter Student Name");
                            name = Console.ReadLine().ToLower();
                            if (name == "") break;
                            Console.WriteLine("Enter Student GPA");
                            bool result = double.TryParse(Console.ReadLine(), out gpa);
                            if (result)
                            {
                                service.AddStudent(service.CreateStudent(name, gpa), students);
                                Console.WriteLine("Student Added");
                            }
                            else
                            {
                                Console.WriteLine("Wrong GPA value");
                            }
                        }


                        break;

                    case "2":
                        Console.WriteLine($"Worst student is {service.WorstStudent(students)}");
                        break;

                    case "3":
                        Console.WriteLine($"Best student is {service.BestStudent(students)}");
                        break;

                    case "4":
                        Console.WriteLine($"GPA of group are {service.AverageGradeByGroup(students)}");
                        break;

                    case "5":
                        System.Environment.Exit(0);
                        break;

                    default:
                        break;
                }
            }

        }
    }
}
